"use strict";
let base = prompt(
  "introduce el número de la base, (decimal:10 o binaria:2):",
  "2"
);

if (base === "10") {
  const num = prompt("introduce un número decimal:");
  function decToBin(dec) {
    return +dec.toString(2);
  }
  console.log(`De decimal a binario: ${decToBin(+num)}`);
}
if (base === "2") {
  const num = prompt("introduce un número binario:");
  function binToDec(bin) {
    return bin
      .split("")
      .reverse()
      .reduce(function (x, y, i) {
        return y === "1" ? x + Math.pow(2, i) : x;
      }, 0);
  }
  console.log(`De binario a decimal: ${binToDec(num)}`);
}
