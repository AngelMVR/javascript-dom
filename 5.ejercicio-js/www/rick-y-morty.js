"use strict";

const url = "https://rickandmortyapi.com/api/";

async function getRickYMortyData(url) {
  const response = await fetch(url);
  const data = await response.json();
  const responseCaracter = await fetch(data.episodes);
  const caracter = await responseCaracter.json();
  const peticion = caracter.results;
  console.log(peticion);
  const mes = peticion.filter((fecha) => {
    return (
      fecha.air_date > "January 1, 2014" && fecha.air_date < "January 31, 2014"
    );
  });
  console.log(mes);

  const character = mes.map((character) => character.characters);

  console.log(character);
  const characterEnero = [];

  for (const personajesmes of character) {
    characterEnero.push(...personajesmes);
  }

  console.log(characterEnero);

  const arrayDeNombresRickYMorty = [];

  for (const character of characterEnero) {
    const characterResponse = await fetch(character);
    const characterData = await characterResponse.json();
    console.log(characterData);
    arrayDeNombresRickYMorty.push(characterData.name);
  }

  // console.log(arrayDeNombresRickYMorty);
  console.log([...new Set(arrayDeNombresRickYMorty)]);
}

getRickYMortyData(url);
