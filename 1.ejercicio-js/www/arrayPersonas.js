"use strict";

const cantidad = prompt("Give me a number");
const apiUrl = `https://randomuser.me/api/?results=${cantidad}`;

async function usersArray() {
  const userRequest = await fetch(apiUrl);
  const userData = await userRequest.json();
  const users = userData.results;

  const arrayOfUsers = [];
  for (const user of users) {
    arrayOfUsers.push(
      `username: ${user.login.username}, name: ${user.name.first}, lastName: ${user.name.last}, gender: ${user.gender}, country: ${user.location.country}, email: ${user.email}, photo: ${user.picture.large}`
    );
  }

  return arrayOfUsers;
}

usersArray(apiUrl).then((responder) => console.log(responder));
