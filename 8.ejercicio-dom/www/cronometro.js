"use strict";

let cronometro;
let cseg = 0;
let seg = 0;
let min = 0;
let hora = 0;

function zer0(number) {
  return number < 10 ? `0${number}` : `${number}`;
}

function clock() {
  if (cseg === 100) {
    cseg = 0;
    seg++;
  }

  if (seg === 60) {
    seg = 0;
    min++;
  }

  if (min === 60) {
    min = 0;
    hora++;
  }

  document.body.innerHTML = `<div>
  <span>${zer0(hora)} : </span>
  <span>${zer0(min)} : </span>
  <span>${zer0(seg)} : </span>
  <span>${zer0(cseg)}</span>
  </div>`;

  cseg++;
}
clock();

//-------------->>Boton de detener<<-------------

const parar = document.createElement("button");
parar.type = "button";
parar.value = "parar";
parar.innerText = "Stop";

const beforeBegin = document.querySelector("body");
beforeBegin.insertAdjacentElement("afterEnd", parar);

parar.className = "parar";
const detener = document.querySelector(".parar");

detener.addEventListener("click", handlerClickDetener, false);

function handlerClickDetener() {
  detenerse();
  function detenerse() {
    clearInterval(cronometro);
  }
}

//--------------->>Boton de Reset<<---------------

const reiniciar = document.createElement("button");
reiniciar.type = "button";
reiniciar.innerText = "Reset";

const beforeBegin1 = document.querySelector("body");
beforeBegin1.insertAdjacentElement("afterend", reiniciar);

reiniciar.className = "reiniciar";
document.querySelector(".reiniciar").addEventListener("click", reinicio);

reiniciar.addEventListener("click", handlerClickReiniciar, false);
function handlerClickReiniciar() {
  () => cronometro.reset();
}

function reinicio() {
  cseg = 0;
  seg = 0;
  min = 0;
  hora = 0;
  document.body.innerHTML = `<div>
  <span>${zer0(hora)} : </span>
  <span>${zer0(min)} : </span>
  <span>${zer0(seg)} : </span>
  <span>${zer0(cseg)}</span>
  </div>`;
}

//------------->>Boton de Play<<--------------

const playB = document.createElement("button");

playB.type = "button";
playB.innerText = "Play";

const beforeBegin2 = document.querySelector("body");
beforeBegin2.insertAdjacentElement("afterend", playB);

playB.className = "play";
document.querySelector(".play").addEventListener("click", play);

function play() {
  cronometro = setInterval(clock, 10);
}
//---------------------------------------------------->>>
marcha = 1; //indicar que esta en marcha
document.cron.boton2.value = "parar"; //Cambiar estado del botón
document.cron.boton1.disabled = false;
