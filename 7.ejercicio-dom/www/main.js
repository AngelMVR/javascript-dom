"use strict";

const body = document.querySelector("body");

const button = document.createElement("button");
button.type = "button";
button.innerText = "Crear cuadrado";
document.body.appendChild(button);
const divBeforeBegin = document.querySelector("body");
divBeforeBegin.insertAdjacentElement("beforeBegin", button);

const selectedButton = document.querySelector("button");
function handlerClick() {
  newSquare();
}
selectedButton.addEventListener("click", handlerClick);

function getRandomColor() {
  let letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.round(Math.random() * 16)];
  }
  return color;
}

function newSquare() {
  const newDiv = document.createElement("div");
  body.appendChild(newDiv);
  newDiv.style.display = "inline-block";
  newDiv.style.width = "100px";
  newDiv.style.height = "100px";
  setInterval(() => {
    newDiv.style.backgroundColor = getRandomColor();
    colorChanges();
  }, 1000);
}
newSquare();
