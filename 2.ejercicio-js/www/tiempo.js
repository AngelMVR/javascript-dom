"use strict";

function fecha() {
  const now = new Date();
  const year = now.getFullYear();
  const month = now.getMonth() + 1;
  const day = now.getDate();
  return `${day}/${month}/${year}`;
}

console.log(fecha());

let sec = 0;
let min = 0;
let hour = 0;
let day = 0;

let timeFromSetup = setInterval(() => {
  if (sec === 60) {
    sec = 0;
    min++;
  }
  if (min === 60) {
    min = 0;
    hour++;
  }
  if (hour === 24) {
    hour = 0;
    day++;
  }
  sec += 5;
  console.log(
    `días: ${zeroBefore(day)} / horas: ${zeroBefore(
      hour
    )} / minutos: ${zeroBefore(min)} / segundos: ${zeroBefore(sec)}`
  );
}, 5000);

function zeroBefore(number) {
  return number < 10 ? `0${number}` : `${number}`;
}
