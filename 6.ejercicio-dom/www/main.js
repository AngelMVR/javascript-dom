"use strict";

let paragraph = document.getElementsByTagName("p");
let span = document.getElementsByTagName("span");
for (const words of paragraph) {
  const text = words.innerHTML;
  const wordsFilter = text.split(" ").filter((word) => word.length > 5);
  words.innerHTML = words.textContent
    .split(" ")
    .map((word) => {
      return wordsFilter.indexOf(word) > 0 ? `<span>${word}</span>` : word;
    })
    .join(" ");
}
for (const word of span) {
  word.style.textDecoration = "underline";
}
